<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Jobs Model
 *
 * @property \App\Model\Table\AppsTable|\Cake\ORM\Association\BelongsTo $Apps
 *
 * @method \App\Model\Entity\Job get($primaryKey, $options = [])
 * @method \App\Model\Entity\Job newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Job[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Job|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Job patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Job[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Job findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class JobsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('jobs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'created_by',
            'propertyName' => 'created_by'
        ]);
    }

    public function getStatus()
    {
        $status_arr = [
            1 => "仮作成",
            2 => "未実行",
            3 => "実行中",
            4 => "完了",
            5 => "キャンセル",
            99 => "エラー",
        ];
        return $status_arr;
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name', "ジョブ名を入力してください。"); //

        $validator
            ->scalar('app_id')
            ->requirePresence('app_id', 'create')
            ->notEmpty('app_id');

        $validator
            ->scalar('app_name')
            ->requirePresence('app_name', 'create')
            ->notEmpty('app_name');

        $validator
            ->integer('count')
            ->allowEmpty('count');

        $validator
            ->scalar('query')
            ->requirePresence('query', 'create')
            ->notEmpty('query');

        $validator
            ->boolean('active_check')
            ->requirePresence('active_check', 'create');

        $validator
            ->boolean('rank_update')
            ->requirePresence('rank_update', 'create');

        $validator
            ->integer('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

	public function find_oldest_record($status)
    {
		$jobs = TableRegistry::get('Jobs');
        $job = $jobs->find('all')->where(['status' => $status])->first();
        return $job;
	}

	public function update_status($job, $status)
    {
		$jobs = TableRegistry::get('Jobs');
        $job->status = $status;
        $jobs->save($job);
        return;
	}

	public function get_site_status($protocols, $domain)
    {
        // 1件ずつ生死チェックする

        $i = 0;
        $sleep_time = 1;
        for ($i = 0; $i < count($protocols); $i++) {
            $url = $protocols[$i].$domain.'/';

            // cURLで生死チェック
            $status = $this->url_response($url);

            // 生が確認できたら終了
            if($status['http_status'] == 200){
                break;
            }
        }
        return $status;
	}

    public static function url_response ($url)
    {
        // ヘッダをGETするcURL

        $status = array('url' => null, 'http_status' => null);

        $curl = curl_init($url);
        $options = array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_TIMEOUT => 30
        );

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        if (curl_error($curl) !== null) {
            echo 'curl_error: '.curl_error($curl)."\n";
        }

        // 取得できたら値を入れる
        if ($response) {
            $status['url'] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            $status['http_status'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }

        curl_close($curl);

        return $status;
    }

	public static function get_api_data($domain, $base_url)
	{
        // Alexaランキングを参照しランクを取ってくる

        $url = "$base_url"."$domain";
        $curl = curl_init($url);
        $sleep_time = 3;

        $options = array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true
        );
        // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, , true);
        curl_setopt_array($curl, $options);
        sleep($sleep_time);
        $response = curl_exec($curl);
        if (curl_error($curl) !== null) {
            echo 'curl_error: '.curl_error($curl)."\n";
        }
        //レスポンスデータをデコード
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $api_result = json_decode($json, true);
        curl_close($curl);
        if (isset($api_result['SD'])) {
            //取ってこれたデータを配列に格納
            if (isset($api_result['SD']['COUNTRY'])) {
                $api_datas['country'] = $api_result['SD']['COUNTRY']['@attributes']['CODE'];
                $api_datas['c_rank'] = $api_result['SD']['COUNTRY']['@attributes']['RANK'];
                $api_datas['global_rank'] = $api_result['SD']['POPULARITY']['@attributes']['TEXT'];
            } else {
                $api_datas['country'] = null;
                $api_datas['c_rank'] = null;
                $api_datas['global_rank'] = $api_result['SD']['POPULARITY']['@attributes']['TEXT'];
            }
        } else {
            $api_datas['country'] = null;
            $api_datas['c_rank'] = null;
            $api_datas['global_rank'] = null;
        }
        return $api_datas;
	}

	public static function get_kintone_json ($params, $api_token, $sub_domain)
	{
        // kintoneからGETしてJSONを配列に直す

        $url = 'https://'.$sub_domain.'.cybozu.com/k/v1/records.json'.$params;
        echo $url."\n";

        // kintoneへcURLでリクエスト送信する
        $curl = curl_init($url);
        $options = array(
            CURLOPT_HTTPHEADER=>array(
                'X-Cybozu-API-Token:'.$api_token
            ),
            CURLOPT_HTTPGET=>true,
            CURLOPT_RETURNTRANSFER => true,
        );

        curl_setopt_array($curl, $options);

        // リクエストを送信
        $response = curl_exec($curl);
        if (curl_error($curl) !== null) {
            echo 'curl_error: '.curl_error($curl)."\n";
        }
        curl_close($curl);

		return $response;
	}

	public static function put_kintone_json ($request_bodies, $api_token, $sub_domain)
	{
        // リクエストボディをJSONにしてPUTする

        json_encode($request_bodies,JSON_UNESCAPED_UNICODE);

        $url = 'https://'.$sub_domain.'.cybozu.com/k/v1/records.json';

        // kintoneへcURLでリクエスト送信する
        $curl = curl_init($url);
        $options = array(
            CURLOPT_HTTPHEADER=>array(
                'X-Cybozu-API-Token:'.$api_token,
                'Content-Type: application/json'
            ),
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($request_bodies,JSON_UNESCAPED_UNICODE),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
        );

        curl_setopt_array($curl, $options);

        // リクエストを送信
        $response = curl_exec($curl);
        if (curl_error($curl) !== null) {
            echo 'curl_error: '.curl_error($curl)."\n";
        }

        curl_close($curl);
        return $response;
	}

	public function update_total_count($job, $total_count)
    {
		// 処理した件数をJobsテーブルに保存
        $jobs = TableRegistry::get('Jobs');
        $job->count = $total_count;
        $jobs->save($job);
        return;
	}
}
