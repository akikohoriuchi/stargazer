<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Job Entity
 *
 * @property int $id
 * @property string $name
 * @property int $app_id
 * @property string $app_name
 * @property int $count
 * @property string $query
 * @property bool $active_check
 * @property bool $rank_update
 * @property int $created_by
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\App $app
 */
class Job extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'app_id' => true,
        'app_name' => true,
        'count' => true,
        'query' => true,
        'active_check' => true,
        'rank_update' => true,
        'created_by' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        // 'app' => true
    ];
}
