<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Google_Client;
use Google_Service_Oauth2;
// アプリケーション設定
define('CONSUMER_KEY', '650034419779-2tvs8etpq7g55dsegcj02v2p2tsmbrfo.apps.googleusercontent.com');
define('CONSUMER_SECRET', 'uuTjUFfd1SPa2UFPKiY2YgrS');

// URL
define('TOKEN_URL', 'https://accounts.google.com/o/oauth2/token');
define('AUTH_URL', 'https://accounts.google.com/o/oauth2/auth');
define('INFO_URL', 'https://www.googleapis.com/oauth2/v1/userinfo');
define('REDIRECT_URI', 'http://192.168.33.60/home/login');

class LoginsController extends AppController
{
    public $callbackUrl = '';

    public function initialize()
    {
        parent::initialize();
        try {
            Configure::config('default', new PhpConfig());
            Configure::load('google_connection_info', 'default', false);
        } catch (\Exception $e) {
            exit($e->getMessage() . "\n");
        }
        $this->Auth->allow(['complete']);

        $this->callbackUrl = Configure::read('callbackUrl');
    }
    /**
     * access_tokenを取得する
     * 
     */
    public function complete()
    {
    	$client = new Google_Client();
    	$client->setAuthConfigFile(CONFIG. '/client_secret.json');
    	$client->setRedirectUri($this->callbackUrl);
    	$client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
    	if (! isset($_GET['code'])) {
    		$auth_url = $client->createAuthUrl();
    		$this->redirect($auth_url);
    	} else {
    		$client->authenticate($_GET['code']);
    		$this->request->session()->write('access_token', $client->getAccessToken());
    		$redirect_uri = REDIRECT_URI;
            return $this->redirect($redirect_uri);
    	}
    }
}
