<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\ORM\TableRegistry;
use Google_Client;
use Google_Service_Oauth2;
class HomeController extends AppController
{
    public $uses = array('Users');
    public $callbackUrl = '';
    // public function initialize()
    // {
    //     parent::initialize();
    //     try {
    //         Configure::config('default', new PhpConfig());
    //         Configure::load('google_connection_info', 'default', false);
    //     } catch (\Exception $e) {
    //         exit($e->getMessage() . "\n");
    //     }

    //     $this->callbackUrl = Configure::read('callbackUrl');

    //     $this->Auth->allow(['login', 'login2']);
    // }

    /**
     * ダッシュボード画面
     * 
     */
    public function index()
    {
        $this->render('index');
    }
    /**
     * ログイン処理
     *
     */
    public function login()
    {
        $client = $this->getGoogleClient();

        if (is_null($this->request->session()->read('access_token'))) {
            return $this->render('login');
        }

       	// ユーザ情報を取得
        $userInfo = $client->verifyIdToken($this->request->session()->read('access_token')['id_token']);

        if(!isset($userInfo['email'])){
            // ログイン失敗　ログイン画面に戻る  
            return $this->render('login');
        }

        $userEntity = $this->Users->find()->where(['mail_addr' => $userInfo['email']])->first();            
        if($userEntity === null && strpos($userInfo['email'], '@respect-pal.jp') === false){
            // ログイン失敗　ログイン画面に戻る
            $this->Flash->set('ログインに失敗しました', ['key' => 'loginFault', 'element' => 'error']);
            return $this->render('login');
        }

        $user = $userEntity;
        if($user === null) {
        	// ユーザ情報がテーブルに保存されていない時の処理
        	// テーブルにユーザ情報を保存
            $user = $this->Users->newEntity();
            $user->name = $userInfo['name'];
            $user->mail_addr = $userInfo['email'];
            $user->admin_flag = false;

            $this->Users->save($user);
        }

        $this->Auth->setUser($user);
        return $this->redirect($this->Auth->redirectUrl());
    }

    public function login2()
    {
        $client = $this->getGoogleClient();
        
        $auth_url = $client->createAuthUrl();
        return $this->redirect($auth_url);
    }

    private function getGoogleClient()
    {
        $client = new Google_Client();
        $client->setAuthConfig(CONFIG. '/client_secret.json');
        $client->setAccessType('offline');
        $client->setIncludeGrantedScopes(true);
        $client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
        $client->setRedirectUri($this->callbackUrl);

        return $client;
    }

    /**
     * ログアウトの処理
     *
     */
    public function logout(){
        $this->redirect($this->Auth->logout());
        $this->request->session()->destroy();
    }
}
