<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\ORM\TableRegistry;
use Google_Client;
use Google_Service_Oauth2;
use App\Controller\AppController;

use App\Controller\Component\OauthComponent;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $uses = array('Users');
    public $callbackUrl = '';


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Oauth');
        try {
            Configure::config('default', new PhpConfig());
        } catch (\Exception $e) {
            exit($e->getMessage() . "\n");
        }

        $this->callbackUrl = Configure::read('callbackUrl');
    }

    /**
     * ログイン処理
     *
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $params = array(
                'client_id' => CLIENT_ID,
                'redirect_uri' => CALLBACK,
                'scope' => 'https://www.googleapis.com/auth/userinfo.profile email',
                'response_type' => 'code',
            );
            return $this->redirect(AUTH_URL . '?' . http_build_query($params));
            exit;
        }
    }

    public function oauth()
    {
        if (!empty($_GET['code'])){
            // Googleから情報を取得
            $userInfo = $this->Oauth->getGoogleInfo($_GET['code']);

            if(!isset($userInfo["email"]) ){
                // ログイン失敗　ログイン画面に戻る  
                $this->Flash->set("ログインに失敗しました", ["key" => "loginFault", "element" => "error"]);
                return $this->render("login");
                exit;
            }
             
            if(!isset($userInfo["hd"]) || $userInfo["hd"] != "respect-pal.jp"){
                // ログイン失敗　ログイン画面に戻る
                $this->Flash->set("ログインに失敗しました", ["key" => "loginFault", "element" => "error"]);
                return $this->render('login');
                exit;
            }

            $userEntity = $this->Users->find()->where(['mail_addr' => $userInfo['email']])->first(); 
            $user = $userEntity;
            if($user === null) {
            // ユーザ情報がテーブルに保存されていない時テーブルにユーザ情報を保存
                $user = $this->Users->newEntity();
                $user->name = $userInfo['name'];
                $user->mail_addr = $userInfo['email'];
                $user->admin_flag = false;
                $this->Users->save($user);
                $userEntity = $this->Users->find()->where(['mail_addr' => $userInfo['email']])->first();
            }
            $this->Auth->setUser($userEntity->id);

            $session = $this->request->session();
            if (!$session->read("app_id")) { //アプリIDがセッションに格納されていなければ、ジョブ一覧ページへ
                return $this->redirect("/jobs");
            } else { //されていれば、ジョブ登録ページへ
                return $this->redirect("/jobs/add");
            }
        } else {
            return $this->redirect("users/login");
        }
    }

    /**
     * ログアウトの処理
     *
     */
    public function logout(){
        $this->redirect($this->Auth->logout());
        $this->request->session()->destroy();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
}
