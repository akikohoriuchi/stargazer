<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Api Controller
 *
 *
 * @method \App\Model\Entity\Api[] paginate($object = null, array $settings = [])
 */
class ApiController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['kintone']);
        // $this->loadComponent('Users');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function kintone()
    {
        // ajaxを使う場合
        // $this->response->cors($this->request)
        //     ->allowOrigin(['*'])
        //     ->allowMethods(['POST'])
        //     ->build();
        // $this->response->body('test');
        $session = $this->request->session();
        $session->write($this->request->query());
        return $this->redirect("/jobs/add");
        exit;
    }
}
