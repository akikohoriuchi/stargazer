<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\ORM\TableRegistry;
use Google_Client;
use Google_Service_Oauth2;
use App\Controller\AppController;


try {
    switch ( env('CAKEPHP_ENV') ) {
        case 'development':
            define('CLIENT_ID', '650034419779-2tvs8etpq7g55dsegcj02v2p2tsmbrfo.apps.googleusercontent.com');
            define('CLIENT_SECRET', 'uuTjUFfd1SPa2UFPKiY2YgrS');
            define('CALLBACK', 'http://stargazer.respect-pal.localhost.org/users/oauth');
            break;
        case 'production':
            define('CLIENT_ID', '1099257495352-amsdojue6g3fulk5id0ufd1s1rd1ls9a.apps.googleusercontent.com');
            define('CLIENT_SECRET', 'avCornbY3fvDWQK-wXH-jrLh');
            define('CALLBACK', 'https://stargazer.respect.work/users/oauth');
            break;
    }
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}



define('TOKEN_URL', 'https://accounts.google.com/o/oauth2/token');
define('INFO_URL', 'https://www.googleapis.com/oauth2/v1/userinfo');
define('AUTH_URL', 'https://accounts.google.com/o/oauth2/auth');

class OauthComponent extends Component
{
	public function getGoogleInfo($code)
	{
   		$params = array(
            'code' => $code,
            'grant_type' => 'authorization_code',
            'redirect_uri' => CALLBACK,
            'client_id' => CLIENT_ID,
            'client_secret' => CLIENT_SECRET,
        );
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
        );

        // POST送信
        $options = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($params),
                'header' => implode("\r\n", $headers),
        ));

        // アクセストークンの取得
        $res_token = file_get_contents(TOKEN_URL, false, stream_context_create($options));
        $token = json_decode($res_token, true);
        if(isset($token['error'])){
            echo 'エラー発生';
            exit;
        }
        $access_token = $token['access_token'];
        $params = array('access_token' => $access_token);

        // ユーザー情報取得
        $res_user = file_get_contents(INFO_URL . '?' . http_build_query($params));
        $userInfo = json_decode($res_user, true);
        return $userInfo;
	}
}