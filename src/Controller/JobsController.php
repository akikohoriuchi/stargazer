<?php
namespace App\Controller;

use App\Controller\AppController;
/**
 * Jobs Controller
 *
 * @property \App\Model\Table\JobsTable $Jobs
 *
 * @method \App\Model\Entity\Job[] paginate($object = null, array $settings = [])
 */
class JobsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */    
    public function initialize()
    {
        parent::initialize();
        $this->set('status_arr', $this->Jobs->getStatus());
    }

    public function index()
    {        
        $this->paginate = [
            'contain' => ['Users'],
            'order' => [
                'id' => 'DESC',
            ]
        ];
        $jobs = $this->paginate($this->Jobs);

        $this->set(compact('jobs'));
        $this->set('_serialize', ['jobs']);
    }

    /**
     * View method
     *
     * @param string|null $id Job id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $job = $this->Jobs->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('job', $job);
        $this->set('_serialize', ['job']);
    }

    public function changeStatus($id = null) 
    {
        $status_changed = [
            2 => 5,
            5 => 2
        ];
        $job = $this->Jobs->get($id);
        $id = $job->id;
        $status = $job->status;
        if ($this->request->is('post')){
            if ($status != 2 && $status != 5) { // 変更は不可
                $this->Flash->success(__('このジョブの実行ステータスは変更できません。'));
                return false;
            } else {
                $job->status = $status_changed[$status];
                if ($this->Jobs->save($job)) {
                    $this->Flash->success(__('実行ステータスを変更しました'));
                    return $this->redirect(['action' => 'index']);
                }
            }         
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    /**
     * access_tokenを取得する※ここにあっていいのか不明
     * 
     */
    public function add()
    {        
        $job = $this->Jobs->newEntity();
        $session = $this->request->session();
        $login_user = $this->Users->get($session->read('Auth.User'))->name;
        $this->set(compact('job'));
        $this->set('_serialize', ['job']);
        $this->set('login_user', $login_user);
        if ($this->request->is(['post'])) {
            $job = $this->Jobs->patchEntity($job, $this->request->getData());

            if ($job->active_check === false && $job->rank_update === false) { 
                $job->setError('active_check', false);
                $job->setError('rank_update', '実行内容を1つ以上選択してください。');
            }
            else {
                // 保存処理とか
                $job->app_id = $session->read("app_id");
                $job->app_name = $session->read("app_name");
                $job->query = $session->read("query");
                $job->created_by = $session->read("Auth.User");
                $job->status = 2;
                if ($this->Jobs->save($job)) {
                    $this->Flash->success(__('ジョブを追加しました。'));
                    return $this->redirect(['action' => 'index']);
                } 
            }

            $this->Flash->error(__('ジョブ追加に失敗しました。もう一度お試しください。'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Job id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $job = $this->Jobs->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $job = $this->Jobs->patchEntity($job, $this->request->getData());
            $job->created_by = $job->created_by->id;
            if ($this->Jobs->save($job)) {
                $this->Flash->success(__('編集内容を保存しました。'));

                return $this->redirect(['action' => "view/$id"]);
            }
            $this->Flash->error(__('編集内容の保存に失敗しました。もう一度お試しください。'));
        }
        $this->set(compact('job'));
        $this->set('_serialize', ['job']);
    }
}
