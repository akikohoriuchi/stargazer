<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Job $job
 */

?>
<h3>
    <?php
        if (mb_strlen($job->name) > 25) {
            $indicatiion = mb_substr($job->name, 0, 25) . "...";
        } else {
            $indicatiion = $job->name;
        }
        echo h($indicatiion);
    ?>
</h3>
<table class="table">
    <colgroup>
        <col style="width:18%;">
    </colgroup>
    <tr>
        <th scope="row"><?= __('Id') ?></th>
        <td><?= $this->Number->format($job->id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('ジョブ名') ?></th>
        <td><?= h($job->name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('アプリID') ?></th>
        <td><?= $this->Number->format($job->app_id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('アプリ名') ?></th>
        <td><?= h($job->app_name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('検索クエリ') ?></th>
        <td><?= h($job->query) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('件数') ?></th>
        <td><?= h($this->Number->format($job->count)) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('作成者') ?></th>
        <td><?= h($job->created_by->name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('実行ステータス') ?></th>
        <td><?= h($status_arr[$job->status]) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('登録日時') ?></th>
        <td><?= h(date('Y/m/d H:i', strtotime($job->created))) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('更新日時') ?></th>
        <td><?= h(date('Y/m/d H:i', strtotime($job->modified))) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('生死チェック') ?></th>
        <td><?= $job->active_check ? __('〇') : __(''); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Alexaランク更新') ?></th>
        <td><?= $job->rank_update ? __('〇') : __(''); ?></td>
    </tr>
</table>

<?php 
    if ($job->status == 2) {
        echo $this->Form->postLink('キャンセル', ['action' => 'changeStatus', $job->id], ['style'=> 'margin-right: 10px', 'class' => 'btn btn-primary', "role"=>"button",'confirm' => __('{0}をキャンセルしますか？', $job->id)]);
    } elseif ($job->status == 5) {
        echo $this->Form->postLink('やっぱり実行', ['action' => 'changeStatus', $job->id], ['style'=> 'margin-right: 10px', 'class' => 'btn btn-primary', 'confirm' => __('{0}をやっぱり実行しますか？', $job->id)]);
    }
    echo $this->HTML->link('編集', ['action' => 'edit', $job->id], ['class' => 'btn btn-primary',"role"=>"button",]);
?>
