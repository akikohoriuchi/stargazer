<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Job[]|\Cake\Collection\CollectionInterface $jobs
 */
?>
<h3><?= __('ジョブ一覧') ?></h3>
<table class="table"　style="table-layout:fixed;width:100%;">
    <colgroup>
        <col style="width:5%;">
        <col style="width:25%;">
        <col style="width:10%;">
    </colgroup>
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ジョブ名') ?></th>
            <th scope="col"><?= $this->Paginator->sort('件数') ?></th>
            <th scope="col"><?= $this->Paginator->sort('生死') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ランク') ?></th>
            <th scope="col"><?= $this->Paginator->sort('作成者') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ステータス') ?></th>
            <th scope="col"><?= $this->Paginator->sort('作成日時') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($jobs as $job): ?>
        <tr>
            <td><?= h($job->id) ?></td>
            <td>
                <?php 
                    if (mb_strlen($job->name) > 25) {
                        $indicatiion = mb_substr($job->name, 0, 25) . "...";
                    } else {
                        $indicatiion = $job->name;
                    }
                    echo $this->Html->link(__($indicatiion), ['action' => 'view', $job->id]);
                ?>                
            </td>
            <td><?= $this->Number->format($job->count) ?></td>
            <td><?= $job->active_check ? __('〇') : __(''); ?></td>
            <td><?= $job->rank_update ? __('〇') : __('') ?></td>
            <td><?= h($job->created_by->name) ?></td>
            <td><?= h($status_arr[$job->status]) ?></td>
            <td><?= h(date('Y/m/d H:i', strtotime($job->created))) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('全{{pages}}ページ中{{page}}ページ目, 全{{count}}レコード中{{current}}レコードを表示')]) ?></p>
</div>

