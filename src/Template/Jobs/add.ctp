<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Job $job
 */
?>
<h3><?= __('ジョブ追加') ?></h3>
<?= $this->Form->create($job) ?>
<table class="table">
    <tr>
        <th scope="row"><?= __('アプリID') ?></th>
        <td><?php echo $this->request->session()->read('app_id')?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('アプリ名') ?></th>
        <td><?php echo $this->request->session()->read('app_name')?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('検索条件') ?></th>
        <td><?php echo $this->request->session()->read('query')?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('作成者') ?></th>
        <td><?= h($login_user) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('ジョブ名') ?></th>
        <td><?php echo $this->Form->control('name', array('label' => false, 'required' => false)); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('実行内容') ?></th>
        <td>
            <?php
            echo $this->Form->control('active_check', array('label' => '生死チェック'));
            echo $this->Form->control('rank_update', array('label' => 'Alexaランク更新'));
            ?>
        </td>
    </tr>
</table>
<?= $this->Form->button(__('追加'), ['class' => 'btn btn-primary']) ?>
<?= $this->Form->end() ?>
