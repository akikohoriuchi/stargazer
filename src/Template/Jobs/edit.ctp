<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Job $job
 */
?>
<h3>
    <?php
        if (mb_strlen($job->name) > 25) {
            $indicatiion = mb_substr($job->name, 0, 25) . "...";
        } else {
            $indicatiion = $job->name;
        }
        echo h($indicatiion);
    ?>
</h3>
<?= $this->Form->create($job) ?>
<table class="table">
    <colgroup>
        <col style="width:18%;">
    </colgroup>
    <tr>
        <th scope="row"><?= __('Id') ?></th>
        <td><?= $this->Number->format($job->id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('ジョブ名') ?></th>
        <td><?php echo $this->Form->control('name', array('label' => false, 'required' => false)); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('アプリID') ?></th>
        <td><?= $this->Number->format($job->app_id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('アプリ名') ?></th>
        <td><?= h($job->app_name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('検索クエリ') ?></th>
        <td><?= h($job->query) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('件数') ?></th>
        <td><?= h($this->Number->format($job->count)) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('作成者') ?></th>
        <td><?= h($job->created_by->name) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('実行ステータス') ?></th>
        <td><?= h($status_arr[$job->status]) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('登録日時') ?></th>
        <td><?= h(date('Y/m/d H:i', strtotime($job->created))) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('更新日時') ?></th>
        <td><?= h(date('Y/m/d H:i', strtotime($job->modified))) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('生死チェック') ?></th>
        <td><?= $job->active_check ? __('〇') : __(''); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Alexaランク更新') ?></th>
        <td><?= $job->rank_update ? __('〇') : __(''); ?></td>
    </tr>
</table>
<?= $this->Form->button(__('更新'), ['class' => 'btn btn-primary']) ?>
<?= $this->Form->end() ?>
