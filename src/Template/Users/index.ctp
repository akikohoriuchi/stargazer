<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<h3><?= __('ユーザー一覧') ?></h3>
<table class="table">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
            <th scope="col"><?= $this->Paginator->sort('名前') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $this->Number->format($user->id) ?></td>
            <td><?= h($user->name) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->numbers() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('全{{pages}}ページ中{{page}}ページ目, 全{{count}}レコード中{{current}}レコードを表示')]) ?></p>
</div>