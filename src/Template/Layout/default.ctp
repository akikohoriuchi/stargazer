<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
// var_dump($this->request->session());
$title = 'Stargazer';
$userid = $this->request->session()->read('Auth.User');
if ($userid != ""){
    $login_str = "ログアウト";
    $action = "/users/logout/";
    $user_view = "\"/users/view/" . $userid . "\"";
}else{
    $login_str = "ログイン";
    $action = "/users/login/";
    $user = "ゲスト";
    $user_view = "";
}
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->css('bootstrap/bootstrap.css'); ?>
    <?= $this->Html->script(['jquery/jquery.js', 'bootstrap/bootstrap.js']); ?>
</head>
<body>
    <nav class="navbar navbar-default" data-topbar role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <?= $this->Html->link(__('Stargazer'), ['controller' => 'Jobs', 'action' => 'index'], ['class' => 'navbar-brand']) ?>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href=<?php echo $action; ?>><?php echo $login_str; ?></a>
                </ul>
            </div>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?php if ($userid != ""): ?>
            <nav class="col-md-2 sidebar" id="actions-sidebar">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-heading">
                            メニュー
                        </div>
                        <ul class="nav nav-pills nav-stacked">
                            <li><?= $this->Html->link(__('ジョブ一覧'), ['controller' => 'Jobs', 'action' => 'index']) ?></li>
                            <li><?= $this->Html->link(__('ユーザー一覧'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                        </ul>
                    </div>
                </div>
            </nav>
        <?php endif; ?>


        <div class="jobs view col-md-9 content-area">
            <?= $this->fetch('content') ?>
        </div>        
    </div>
    <footer>
    </footer>
</body>
</html>
