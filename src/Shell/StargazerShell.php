<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Sunra\PhpSimple\HtmlDomParser;
use Cake\Core\Exception\Exception;


class StargazerShell extends Shell
{
    public function initialize()
    {
    	parent::initialize();
    	$this->loadModel('Jobs');
    }

    public function main()
    {
    	try {
            // プロセスファイルが存在しているか確認
            $dir = new Folder(TMP.'/processfile');
            if ($files = $dir->find('process.php')) {
                $this->out('プロセスファイルが存在しているので実行を停止しました。');
                die;
            }
            $this->out('バッチを実行します。');

            // プロセスファイルを作成
            $file = new File(TMP.'/processfile/process.php', true, 770);
            $this->out('プロセスファイルを作成しました。');

            // ジョブテーブルからステータス2（未実行）のジョブを取得（1件）
            $job = $this->Jobs->find_oldest_record(2);

            if (empty($job)) {
                $this->out('未実行のジョブはありません。バッチの実行を停止しました。');
                $file->delete();
                $this->out('プロセスファイルを削除しました。');
                die;
            }
            $this->out('ジョブを実行します。実行するジョブ名：'.$job->name);

            // ジョブのステータスを3（実行中）に更新
            $this->Jobs->update_status($job, 3);

            // kintoneに接続してクエリのレコードを500件取得
            $app_id = $job->app_id;
            $query = $job->query;
            $active_check = $job->active_check;
            $rank_update = $job->rank_update;
            $offset_get = 0;
            $offset_put = 0;

            $total_count_flug = true;
            $stop_flug = false;

            while (!$stop_flug) {

                $params = '?app='.$app_id.'&query='.urlencode($query.'order by $id desc limit 500 offset '.$offset_get).
                    '&fields[0]=$id'.
                    '&fields[1]='.urlencode('domain').
                    '&fields[2]='.urlencode('サイト国籍').
                    '&totalCount=true';
                $api_token = 'FcczDr2tyOPXrF0k2yY2i0nbuAJWhqZJXBNRYXt4';
                $sub_domain = 'azitas';
                $array_response = array(
                    'totalCount'=>null,
                    'records'=>null
                );

                $response = $this->Jobs->get_kintone_json($params, $api_token, $sub_domain);
                $array_response = json_decode($response, true);
                var_dump($response);

                $offset_get_plus_1 = $offset_get+1;
                $records_count = $offset_get+count($array_response['records']);

                if (isset($array_response["message"]) || empty($response)) {
                    throw new Exception($offset_get_plus_1.'～'.$records_count.'件目でkintoneからデータを取ってこれませんでした。 message: '.$array_response["message"],1);
                }

                $this->out($array_response['totalCount'].'件中'.$offset_get_plus_1.'～'.$records_count.'件目を処理します。');

                // 1周目のみ処理するトータルカウントをテーブルに記入
                if ($total_count_flug === true) {
                    $this->Jobs->update_total_count($job, $array_response['totalCount']);
                    $total_count_flug = false;
                }

                // 実行内容を判別しチェックを実行します
                $request_bodies = array('app'=>$app_id, 'records'=>'');
                // 100件ずつ5回PUT
                for ($j=0; $j<5; $j++) {
                    for ($i=0; $i < 100; $i++) {
                        $now_count = $j*100+$i+1+$offset_get;

                        echo "\n";
                        $i_plus_offset_put = $i+$offset_put;
                        $domain = $array_response['records'][$i_plus_offset_put]["domain"]['value'];
                        $request_bodies['records'][$i]['id'] = $array_response['records'][$i_plus_offset_put]['$id']['value'];
                        $status = array('http_status'=>null, 'url'=>null, '0'=>null);

                        $this->out($array_response['totalCount'].'件中　'.$now_count.'　件目: '.$domain);

                        // ドメインがないものはスキップ
                        if (empty($array_response['records'][$i_plus_offset_put]["domain"]['value'])) {
                            $this->out('ドメインがないのでスキップします。');
                            if ($now_count == $array_response['totalCount']) {
                                $stop_flug = true;
                                $this->out('ストップフラグがオンになりました。');
                                break;
                            }
                            continue;
                        }

                        // 生死チェックをする場合
                        if ($active_check == 1) {

                            $this->out('生死チェックをします。');
                            // JPチェックに値が入っているか確認する。値が入っているものはJPチェックしない。
                            if ($array_response['records'][$i_plus_offset_put]["サイト国籍"]['value'] == '（未確認）' || $array_response['records'][$i_plus_offset_put]["サイト国籍"]['value'] === NULL) {
                                // ドメインが.jpのものは「日本」
                                if (strpos($array_response['records'][$i_plus_offset_put]["domain"]['value'], '.jp') !== false) {
                                    $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '日本';
                                } else {
                                    $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '（未確認）';
                                }
                            }
                            // ドメインが.jpのものは「日本」（これまでのまちがい修正用）
                            // if (strpos($array_response['records'][$i_plus_offset_put]["domain"]['value'], '.jp') !== false) {
                            //     $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '日本';
                            // }

                            // 生死チェック HTTPステータスコードより判定
                            //URLリダイレクト対策取られてない時の対策用
                            $protocols = array('http://', 'https://', 'http://www.', 'https://www.');

                            $time_start = microtime(true);

                            // domainよりURLを作りサイト生死チェック
                            $status = $this->Jobs->get_site_status($protocols, $domain);

                            $this->out('URL：'.$status['url']);
                            $this->out('HTTPステータス：'.$status['http_status']);

                            //$status['http_status'] が200のときは生
                            if ($status['http_status'] == 200) {
                                $request_bodies['records'][$i]['record']["サイト生死"]['value'] = '生';

                                // ドメインで判別つかなかったサイトをlang属性でJPチェック
                                if ($request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '（未確認）') {
                                    $html = HtmlDomParser::file_get_html($status['url']);
                                    if ($html) {
                                        foreach($html->find("html") as $element){
                                            if ($element->lang == 'ja') {
                                                $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '日本';
                                            } else if ($element->lang == NULL || $element->lang == '') {
                                                $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '判別不可';
                                            } else {
                                                $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '海外';
                                            }
                                        }
                                    } else {
                                        echo 'HTML確認できませんでした！'."\n";
                                        $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '判別不可';
                                    }
                                }
                            } else {
                                $this->out('生きていることが確認できませんでした。');
                                $request_bodies['records'][$i]['record']["サイト生死"]['value'] = '死';
                                // 死のサイトはJPチェックを判別不可にする
                                $request_bodies['records'][$i]['record']["サイト国籍"]['value'] = '判別不可';
                            }

                            // 日付取得
                            $request_bodies['records'][$i]['record']["生死チェック日"]['value'] = date('Y-m-d');

                            $time = microtime(true) - $time_start;
                            $this->out('生死チェック完了しました。タイム：'.$time.'秒');

                        }
                        // ランク更新をする場合
                        if ($rank_update == 1) {

                            $this->out('ランク更新をします。');
                            $base_url = "http://data.alexa.com/data?cli=10&url=";
                            $time_start = microtime(true);

                            $api_datas = $this->Jobs->get_api_data($domain,$base_url);

                            $request_bodies['records'][$i]['record']["Alexa世界ランク"]['value'] = $api_datas['global_rank'];
                            $request_bodies['records'][$i]['record']["Alexa国別ランク(ランク)"]['value'] = $api_datas['c_rank'];
                            $request_bodies['records'][$i]['record']["Alexa国別ランク(国名)"]['value'] = $api_datas['country'];
                            $request_bodies['records'][$i]['record']["Alexaランク更新日"]['value'] = date('Y-m-d');

                            $time = microtime(true) - $time_start;
                            $this->out("最新のランクの取得が完了しました。タイム：".$time.'秒');

                        }
                        var_dump($request_bodies['records'][$i]);
                        if ($now_count == $array_response['totalCount']) {
                            $stop_flug = true;
                            $this->out('ストップフラグがオンになりました。');
                            break;
                        }
                    }

                    // 100件ごとにPUTリクエストを送信
                    $put_response = $this->Jobs->put_kintone_json($request_bodies, $api_token, $sub_domain);
                    $array_put_response = json_decode($put_response, true);
                    var_dump($array_put_response);
                    if (isset($array_put_response["message"]) || empty($put_response)) {
                        throw new Exception($now_count."件目でkintoneにデータを送れませんでした。message: ".$array_put_response["message"], 1);
                    }

                    $this->out('kintoneに送信しました！');
                    $offset_put += 100;

                    if ($stop_flug) {
                        $this->out('ストップフラグで止めました。');
                        break;
                    }
                }
                $offset_put = 0;
                $offset_get += 500;
            }

            // ジョブのステータスを4（完了）に更新
            $this->Jobs->update_status($job, 4);
            $this->out('ジョブを完了しました。完了したジョブ名：'.$job->name);

            // プロセスファイルを削除
            $file->delete();
            $this->out('プロセスファイルを削除しました。');

        } catch (Exception $e) {
            $this->out($e->getMessage());
            $job = $this->Jobs->find_oldest_record(3);
            $this->Jobs->update_status($job, 99);
            error_log(date("Y-m-d H:i:s")." ".$e->getMessage()."\n", 3, TMP."cache/error.log");
            $file->delete();
            $this->out('プロセスファイルを削除しました。');
        }

    }

    public static function parse()
    {
        $html = HtmlDomParser::file_get_html("http://m-king.co.jp/index2.php");
        foreach($html->find("html") as $element){
            echo $element->lang;
        }

    }

    public static function rank()
    {
        //API叩いて、新しいランクを取得

        try {
            $domain = 'mcdonalds.co.jp';
            $base_url = "http://data.alexa.com/data?cli=10&url=";

            $time_start = microtime(true);
            $sleep_time = 3;
            echo 'ランクチェックするドメイン：'.'mcdonalds.co.jp';
            $url = "$base_url"."$domain";
            $curl = curl_init($url);
            //curlの設定
            $options = array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true
            );
            curl_setopt_array($curl, $options);
            sleep($sleep_time);
            $response = curl_exec($curl);
            //レスポンスデータをデコード
            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $api_result = json_decode($json, true);
            curl_close($curl);
            if (isset($api_result['SD'])) {
                //flagの数値は取得できなかったランク数を表す
                if (isset($api_result['SD']['COUNTRY'])) {
                    $api_datas['country'] = $api_result['SD']['COUNTRY']['@attributes']['CODE'];
                    $api_datas['c_rank'] = $api_result['SD']['COUNTRY']['@attributes']['RANK'];
                    $api_datas['global_rank'] = $api_result['SD']['POPULARITY']['@attributes']['TEXT'];
                } else {
                    $api_datas['country'] = null;
                    $api_datas['c_rank'] = null;
                    $api_datas['global_rank'] = $api_result['SD']['POPULARITY']['@attributes']['TEXT'];
                }
            } else {
                $api_datas['country'] = null;
                $api_datas['c_rank'] = null;
                $api_datas['global_rank'] = null;
            }

            $records = [];

            $records['record']["Alexa世界ランク"]['value'] = $api_datas['global_rank'];
            $records['record']["Alexa国別ランク(ランク)"]['value'] = $api_datas['c_rank'];
            $records['record']["Alexa国別ランク(国名)"]['value'] = $api_datas['country'];
            $records['record']["Alexaランク更新日"]['value'] = date('Y-m-d');


            $time = microtime(true) - $time_start;
            echo "{$time} 秒"."\n";

            echo "最新のランクの保存が完了しました。"."\n";
            var_dump($records);
        } catch (Exception $e) {
            echo $e;
        }
    }

    public static function count ()
    {
        $array_response['totalCount'] = 10;
        $offset_get = 0;
        $array_response['records'] = array(1,1,1,1,1);
        do
        {
            echo $offset_get+count($array_response['records']);
            $offset_get += 1;
        } while ($array_response['totalCount'] >= $offset_get+count($array_response['records']));
    }

    public static function curl ()
    {
        // ヘッダをGETするcURL

        $sleep_time = 1;
        $url = 'https://www.kobuchihama.com/';

        $curl = curl_init($url);
        $options = array(
            CURLOPT_HEADER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_NOPROGRESS => false,
        );

        curl_setopt_array($curl, $options);

        sleep ($sleep_time);

        //リクエストを送信
        $responses = curl_exec($curl);
        echo 'curl_error: '.curl_error($curl)."\n";
        $info = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        var_dump($info);
        curl_close($curl);

        // var_dump($responses);
    }

    public static function active()
    {
        $protocols = array('http://', 'https://', 'http://www.', 'https://www.');
        $domain = 'kobuchihama.com';

        $status['http_status'] = null;
        $status['url'] = null;
        for ($i = 0;$i < count($protocols);$i++) {
            $responses[0] = null;
            $url = $protocols[$i].$domain.'/';

            // cURLで生死チェック
            echo $url."\n";

            $curl = curl_init($url);
            $options = array(
                CURLOPT_HEADER => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30
            );

            curl_setopt_array($curl, $options);

            //リクエストを送信
            $response = curl_exec($curl);
            if ($response) {
                $status['url'] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
                $status['http_status'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            }

            curl_close($curl);

            var_dump($responses);
            var_dump($status);

            // 生が確認できたら終了
            if($status['http_status'] == 200){
                break;
            }
        }
    }

    public static function kin ()
    {

        $offset_get = 0;
        $offset_put = 0;
        $k = 0;
        $stop_flug = false;
        $app_id = 227;
        $query = 'ドメイン like "ai.jp"';

        while (!$stop_flug) {
            $params = '?app='.$app_id.'&query='.urlencode($query.'order by $id desc limit 500 offset '.$offset_get).
                '&fields[0]=$id'.
                '&fields[1]='.urlencode('domain').
                '&fields[2]='.urlencode('JPチェック').
                '&totalCount=true';
            $api_token = 'FcczDr2tyOPXrF0k2yY2i0nbuAJWhqZJXBNRYXt4';
            $sub_domain = 'azitas';
            $url = 'https://'.$sub_domain.'.cybozu.com/k/v1/records.json'.$params;
            echo $url."\n";
            $sleep_time = 1;

            // kintoneへcURLでリクエスト送信する
            $curl = curl_init($url);
            $options = array(
                CURLOPT_HTTPHEADER=>array(
                    'X-Cybozu-API-Token:'.$api_token
                ),
                CURLOPT_HTTPGET=>true,
                CURLOPT_RETURNTRANSFER => true,
            );

            curl_setopt_array($curl, $options);

            sleep ($sleep_time);

            // リクエストを送信
            $response = curl_exec($curl);
            if (curl_error($curl) !== null) {
                echo 'curl_error: '.curl_error($curl);
            }
            curl_close($curl);

            if (empty($response)) {
                echo 'kintoneからデータを取ってこれませんでした。';
                // $job = $this->find_oldest_record(3);
                // $this->update_status(99);
                die;
            }

            $array_response = json_decode($response, true);
            var_dump(json_last_error());

            echo '接続問題なしでーす。'."\n";
            for ($i=0; $i < 5; $i++) {
                for ($j=0; $j < 100; $j++) {
                    $now_count = $j+$offset_put+1;
                    echo $now_count;
                    var_dump($array_response['records'][$j]);
                    if ($now_count == $array_response['totalCount']) {
                        $stop_flug = true;
                        echo 'ストップフラグがオンになりました。';
                        break;
                    }
                }
                $offset_put += 100;
                echo '送信'."\n";

                if ($stop_flug) {
                    echo 'ストップフラグで止めました。';
                    break;
                }
            }
            $offset_get += 500;
            echo '500くぎり'."\n";
        }
        echo 'おわり'."\n";
    }
    public static function while ()
    {

        $offset_get = 0;
        $offset_put = 0;
        $k = 0;
        $stop_flug = false;
        $app_id = 227;
        $query = 'id >= "300000" and id <= "300511"';

        while (!$stop_flug) {
            $array_response['totalCount'] = 512;
            $array_response[0] = 500;
            $array_response[1] = 12;
            echo '接続問題なしでーす。'."\n";
            for ($i=0; $i < 5; $i++) {
                for ($j=0; $j < 100; $j++) {
                    $now_count = $i*100+$j+1;
                    echo $now_count;
                    if ($now_count+$offset_get == $array_response['totalCount']) {
                        $stop_flug = true;
                        echo 'ストップフラグがオンになりました。';
                        break;
                    }
                }
                $offset_put += 100;
                echo '送信'."\n";

                if ($stop_flug) {
                    echo 'ストップフラグで止めました。';
                    break;
                }
            }
            $offset_put = 0;
            $offset_get += 500;
            echo '500くぎり'."\n";
        }
        echo 'おわり'."\n";
    }

    public function try()
    {

        try{
            $dir = new Folder(APP.'/Shell/processfile');
            if ($files = $dir->find('process.php')) {
                $this->error('プロセスファイルが存在しているので実行を停止しました。');
            }
            $this->out('バッチを実行します。');

            // プロセスファイルを作成
            $file = new File(APP.'/Shell/processfile/process.php', true, 770);
            $this->out('プロセスファイルを作成しました。');

            // ジョブテーブルからステータス2（未実行）のジョブを取得（1件）
            $job = $this->Jobs->find_oldest_record(60);
        }catch(Exception $e){
            echo $e."\n";
            $job = $this->find_oldest_record(3);
            $this->update_status(99);
            $error_file = new File(APP.'/logs/stargazer/'.date().'errorlog.txt', true, 777);
            $error_file->write($e);
            $error_file->close();
            $dir = new Folder(APP.'/Shell/processfile');
            if ($file = $dir->find('process.php')){
                $file->delete();
                $this->out('プロセスファイルを削除しました。');
            }

        }
    }
}